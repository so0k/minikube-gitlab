# Gitlab Kubernetes / Helm - Gitlab demo

Setting up a sample Gitlab on Minikube using Helm

## Minikube set up (MacOS)

Using a single node Kubernetes cluster running with hyperkit driver and dnsmasq to ease DNS management.

If laptop already had Minikube set up, upgrade as follows:

```bash
minikube delete # remove old VM - was k8s 1.6 in my case
brew reinstall minikube # reinstall minikube
```

Ensure [hyperkit driver is installed](https://github.com/kubernetes/minikube/blob/master/docs/drivers.md#hyperkit-driver)

```bash
minikube start --vm-driver hyperkit
```

Install helm and run `helm init`

### Troubleshooting Minikube set up

- minikube was stuck creating addon-manager deployment
- `eval $(minikube docker-env)` and `docker ps` indicated no containers were starting
- `minikube ssh` and `journalctl -u localkube -f` indicated DNS issues from within minikube
- GitHub issues pinpointed [dnsmasq](https://github.com/kubernetes/minikube/issues/1561#issuecomment-353711333) as the culprit
- used `brew services` to `stop` DNSmasq and confirmed after restarting minikube, everything worked
- reconfigured DNSMasq to listen for minikube requests (i.e. `minikube ip`: 192.168.64.5) and use the Wifi DNS: 192.168.1.1

```
address=/dev/127.0.0.1
address=/k8s.local/192.168.64.5
no-resolv
strict-order
server=192.168.1.1
server=8.8.8.8
server=8.8.4.4
listen-address=192.168.64.1
listen-address=127.0.0.1
```

Confirm dnsmasq resolver config for `local` domain is correct [see guide](https://passingcuriosity.com/2013/dnsmasq-dev-osx/) (additional step missing from guide is to add 172.0.01 as DNS entry in network set up)

- [confirm dnsmasq is listening](https://github.com/drduh/macOS-Security-and-Privacy-Guide/issues/67)
- Try running dnsmasq manually (sometimes dnsmasq fails because minikube did not create virtual bridge interface for `192.168.64.1` and dnsmasq can not bind to it)
- try also flushing cache `dscacheutil -flushcache`

[Verify setup](test/README.md)

## Gitlab Set Up

Install Postgres on cluster (not HA)

```
helm install stable/postgresql -n gitlabdb --set postgresUser=gitlab --set postgresPassword=gitlabpass --set postgresDatabase=gitlabdb
```

Install Redis on cluster (not HA)

```
helm install stable/redis -n gitlabcache --set redisPassword="gW8YclVysf"
```

Decided to use: https://github.com/samsung-cnct/chart-gitlab-ce

git clone, 

```bash
CHART_VER=$(git describe --tags --abbrev=0 | sed 's/^v//') CHART_REL=0 envsubst < build/Chart.yaml.in > gitlab-ce/Chart.yaml
```

go to `gitlab-ce` chart dir


Set up Self signed TLS:

[ref](https://github.com/kubernetes/minikube/issues/1621)

```bash
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout test-tls.key -out test-tls.crt -subj "/CN=gitlab.k8s.local"
```


```bash
helm install -n gitlab -f minikube.yaml --set ingress.tls.key=$(base64 test-tls.key) --set ingress.tls.certificate=$(base64 test-tls.crt) .
```
