# Testing the minikube / dnsmasq set up

Confirm cluster is up

```bash
kubectl cluster-info
```

Create nginx deployment (imperatively)

```bash
kubectl run nginx --image nginx:1.13-alpine
```

Create nginx service

```bash
kubectl expose deploy nginx --port 80
```

Create ingress

```bash
kubectl create -f ./ingress.yaml
```

http://test.k8s.local should now show the nginx welcome page

Everything works!
